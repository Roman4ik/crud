import React, { useState, Fragment, useEffect } from 'react'
import 'antd/dist/antd.css'
import {nanoid} from 'nanoid'
import './app.css'
import { ReadOnlyRow } from './ReadOnlyRow'
import { EditableRow } from './EditableRow'
import  axios  from 'axios';
import { Button, Input } from 'antd'




function App() {
    console.log('rrrrrrrendeeeeeeeeeeeeer')

    const [contacts, setContacts] = useState([])
    const [addFormData, setAddFormData] = useState({
        name: '',
        age: '',
        email: ''
    })

    // const inputRef = React.useRef()
    // console.log(inputRef.current)

    // useEffect(() => {
    //     console.log(inputRef.current)
    // },[addFormData])
 

    useEffect(() => {
            axios.get('http://178.128.196.163:3000/api/records')
        // .then((res) => console.log(res.data))
        .then((res) => setContacts(res.data))
    }, [contacts.length])

    console.log(contacts)

    const [editFormData, setEditFormData] = useState({
        name: '',
        age: '',
        email: ''
    })

    const [editContactId, setEditCOntactId] = useState(null)



    const handleAddFormChange = (event) => {
        event.preventDefault()
        const fieldName = event.target.getAttribute('name')
        const fieldValue = event.target.value

        const newFormData = {...addFormData}
        newFormData[fieldName] = fieldValue

        setAddFormData(newFormData)
    }

    const handleEditFormChange = (event) => {
        const fieldName = event.target.getAttribute('name')
        const fieldValue = event.target.value

        const newFormData = {...editFormData}
        // console.log('edit data', editFormData)
        // console.log('new form data',newFormData)
        newFormData[fieldName] = fieldValue

        setEditFormData(newFormData)
    }

    const handleAddFormSubmit = async (event) => {
        event.preventDefault()


        const newContact = {
            data: {
                name: addFormData.name,
                age: addFormData.age,
                email: addFormData.email
            }
        }
          const response = await axios.put('http://178.128.196.163:3000/api/records', newContact)
           setContacts([...contacts, response.data.data])
           setAddFormData({
            name: '',
            age: '',
            email: ''
           })
    }

    const handleEditFormSubmit = async (event) => {
        event.preventDefault()

        const editedContact = {
            data: {
                name: editFormData.name,
                age: editFormData.age,
                email: editFormData.email
            }

        }

        const newContacts = [...contacts]
        const index = contacts.findIndex((contact) => contact.id === editContactId)

        newContacts[index] = editedContact
        const response = await axios.post(`http://178.128.196.163:3000/api/records/${editContactId}`, editedContact)
        setContacts([...contacts, response.data.data])
        setEditCOntactId(null)
    }

    const handleEditClick = (event, contact) => {
        event.preventDefault()
        console.log(contact)
        setEditCOntactId(contact._id)
    
        const formValues = {
            name: contact.data.name,
            age: contact.data.age,
            email: contact.data.email
        }

        setEditFormData(formValues)
    }

    const handleCancelClick = () => {
        setEditCOntactId(null)

    }

    const handleDeleteClick = async (contactId) => {
        console.log('contact id',contactId)
      await axios.delete(`http://178.128.196.163:3000/api/records/${contactId}`)
        setContacts([...contacts].filter((el) => el._id !== contactId))

    }



    return (
    <div className='app-container'>
        <form onSubmit={handleEditFormSubmit} >
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {contacts.map((el) =>
                <Fragment key={nanoid()}>
                    {editContactId === el._id
                    ? <EditableRow  setEditFormData={setEditFormData}  el={el} editFormData={editFormData} handleEditFormChange={handleEditFormChange} handleCancelClick={handleCancelClick} />
                    : <ReadOnlyRow  el={el} handleEditClick={handleEditClick} handleDeleteClick={handleDeleteClick} />}
                </Fragment> 

                )}
            </tbody>
        </table>
        </form>
        <h2>Add contact</h2>
        <form onSubmit={handleAddFormSubmit} className='addForm'>
            <Input 
                type="text"
                name="name"
                required="required"
                placeholder="Enter a name..."
                onChange={handleAddFormChange}
                value={addFormData.name} />
            <Input
                type="text"
                name="age"
                required="required"
                placeholder="Enter an age..."
                onChange={handleAddFormChange}
                value={addFormData.age} />
            <Input
                type="text"
                name="email"
                required="required"
                placeholder="Enter a email..."
                onChange={handleAddFormChange}
                value={addFormData.email} />
            <Button  style={{margin: '5px auto'}} type='primary' htmlType="submit" >Add contact</Button>
        </form>
    </div>

    )
}

export default App
