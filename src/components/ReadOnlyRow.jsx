import React from 'react'
import { Button } from 'antd'

export const ReadOnlyRow = (props) => {
  if(props.el.data) {
  return (
    <tr>
      <td>{props.el.data.name}</td>
      <td>{props.el.data.age}</td>
      <td>{props.el.data.email}</td>
      <td>
        <Button type='button' style={{backgroundColor: '#ffd666', marginRight: '5px'}} onClick={(event) => props.handleEditClick(event, props.el)} >Edit</Button>
        <Button type='primary' danger onClick={() => props.handleDeleteClick(props.el._id)} >Delete</Button>
      </td>
    </tr>
  )
  }
  return null
}
