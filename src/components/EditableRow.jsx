import React from 'react'
import { Button } from 'antd'




export const EditableRow = (props) => {


  return (
    <tr>
      <td>
        <input 
          type='text'
          required='required'
          placeholder='Enter a name...'
          name='name'
          value={props.editFormData.name}
          onChange={props.handleEditFormChange} />
      </td>
      <td>
      <input 
          type='text'
          required='required'
          placeholder='Enter an age...'
          name='age'
          value={props.editFormData.age}
          onChange={props.handleEditFormChange} />
      </td>
      <td>
        <input
          type='email'
          required='required'
          placeholder='Enter a email...'
          name='email'
          value={props.editFormData.email}
          onChange={props.handleEditFormChange} />
      </td>
      <td>
      <Button  style={{marginRight: '5px', backgroundColor: '#52c41a'}} type='primary' htmlType="submit" >Save</Button>
      <Button type='primary' danger onClick={() => props.handleDeleteClick(props.el._id)} >Cancel</Button>
      </td>
    </tr>
  )
}
